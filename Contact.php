<?php

namespace Sootlib\XXXChange;

use JsonSerializable;

class Contact implements JsonSerializable {

    private $address;
    private $username;

    public function __construct($address, $username){
        $this->address = $address;
        $this->username = $username;
    }

    public function get_address(){
        return $this->address;
    }

    public function get_username(){
        return $this->username;
    }

    function jsonSerialize() {
        return get_object_vars($this);
    }
}