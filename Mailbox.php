<?php

namespace Sootlib\XXXChange;

use DateInterval;
use DateTime;
use Exception;
use jamesiarmes\PhpEws\ArrayType\ArrayOfRecipientsType;
use jamesiarmes\PhpEws\ArrayType\ArrayOfStringsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfAllItemsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfBaseFolderIdsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfBaseItemIdsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfFieldOrdersType;
use jamesiarmes\PhpEws\Client;
use jamesiarmes\PhpEws\Enumeration\BodyTypeType;
use jamesiarmes\PhpEws\Enumeration\CalendarItemCreateOrDeleteOperationType;
use jamesiarmes\PhpEws\Enumeration\DefaultShapeNamesType;
use jamesiarmes\PhpEws\Enumeration\DisposalType;
use jamesiarmes\PhpEws\Enumeration\DistinguishedFolderIdNameType;
use jamesiarmes\PhpEws\Enumeration\ImportanceChoicesType;
use jamesiarmes\PhpEws\Enumeration\ItemQueryTraversalType;
use jamesiarmes\PhpEws\Enumeration\MessageDispositionType;
use jamesiarmes\PhpEws\Enumeration\ResponseClassType;
use jamesiarmes\PhpEws\Request\CreateItemType;
use jamesiarmes\PhpEws\Request\DeleteItemType;
use jamesiarmes\PhpEws\Request\FindItemType;
use jamesiarmes\PhpEws\Request\GetItemType;
use jamesiarmes\PhpEws\Request\SendItemType;
use jamesiarmes\PhpEws\Type\BodyType;
use jamesiarmes\PhpEws\Type\CalendarItemType;
use jamesiarmes\PhpEws\Type\CalendarViewType;
use jamesiarmes\PhpEws\Type\ConstantValueType;
use jamesiarmes\PhpEws\Type\ContainsExpressionType;
use jamesiarmes\PhpEws\Type\DistinguishedFolderIdType;
use jamesiarmes\PhpEws\Type\EmailAddressType;
use jamesiarmes\PhpEws\Type\FieldOrderType;
use jamesiarmes\PhpEws\Type\IndexedPageViewType;
use jamesiarmes\PhpEws\Type\ItemIdType;
use jamesiarmes\PhpEws\Type\ItemResponseShapeType;
use jamesiarmes\PhpEws\Type\MessageType;
use jamesiarmes\PhpEws\Type\PathToUnindexedFieldType;
use jamesiarmes\PhpEws\Type\RestrictionType;
use jamesiarmes\PhpEws\Type\SingleRecipientType;
use jamesiarmes\PhpEws\Type\TargetFolderIdType;
use Sootlib\XXXChange\Structs\Email_Id;
use Sootlib\XXXChange\Structs\Email_Query;
use Sootlib\XXXChange\Structs\Email_Query_Limit;
use Sootlib\XXXChange\Structs\Event_Query;
use Sootlib\XXXChange\Structs\Header;

class Mailbox {

    private $client;

    public function __construct(Client $client){
        $this->client = $client;
    }

    public function get_most_recent_mail() {
        return $this->get_mail_by_index(0);
    }

    public function get_x_recent_mails($x){
        $limit = new Email_Query_Limit();
        $limit->limit = $x;
        $limit->offset = 0;
        $query = new Email_Query();
        $query->limit = $limit;
        $assocs = $this->get_mails_as_assoc($query);
        $emails = array();
        foreach($assocs as $a){
            $email = $this->assoc_to_email($a);
            array_push($emails, $email);
        }
        $emails = array_reverse($emails);
        return $emails;
    }

    private function get_mail_by_index($n){
        $mailIds = array();
        $limit = new Email_Query_Limit();
        $limit->limit = 1;
        $limit->offset = $n;
        $query = new Email_Query();
        $query->limit = $limit;
        $messages = $this->get_mails_as_assoc($query);
        return $this->assoc_to_email($messages[0]);
    }

    public function get_mails(Email_Query $restirction = NULL){
        $assoc = $this->get_mails_as_assoc($restirction);
        $mails = array();
        foreach($assoc as $a){
            $a = $this->assoc_to_email($a);
            array_push($mails, $a);
        }
        return $mails;
    }

    public function send_mail(Email $email){
        $request = new CreateItemType();
        $request->Items = new NonEmptyArrayOfBaseItemIdsType();
        $request->MessageDisposition = MessageDispositionType::SAVE_ONLY;

        $message = new MessageType();
        //subject
        $message->Subject = $email->get_title();
        //to
        $message->ToRecipients = new ArrayOfRecipientsType();
        foreach($email->get_reciever()->get_contacts() as $r){
            $recipients = new EmailAddressType();
            $recipients->Name = $r->get_username();
            $recipients->EmailAddress = $r->get_address();
            $message->ToRecipients->Mailbox[] = $recipients;
        }
        //from
        $message->From = new SingleRecipientType();
        $message->From->Mailbox = new EmailAddressType();
        $message->From->Mailbox->EmailAddress = $email->get_sender()->get_address();
        //bcc
        $message->BccRecipients = new ArrayOfRecipientsType();
        foreach($email->get_bcc() as $r){
          $bcc_recipent = new EmailAddressType();
          $bcc_recipent->Name = $r;
          $bcc_recipent->EmailAddress = $r;
          $message->BccRecipients->Mailbox[] = $bcc_recipent;
        }
        //body
        $message->Body = new BodyType();
        $message->Body->BodyType =  BodyTypeType::HTML;
        $message->Body->_ = $email->get_body();
        $request->Items->Message[] = $message;
        $response = $this->client->CreateItem($request);
        $response_messages = $response->ResponseMessages->CreateItemResponseMessage;
        foreach($response_messages as $r_message){
            if($r_message->ResponseClass != ResponseClassType::SUCCESS){
                throw Exception("Error : Message Failed To Save as Draft");
            }
            foreach($r_message->Items->Message as $item){
                $send_request = new SendItemType();
                $send_request->SaveItemToFolder = TRUE;
                $send_request->ItemIds = new NonEmptyArrayOfBaseItemIdsType();
                $item_ref = $item->ItemId;
                $send_request->ItemIds->ItemId[] = $item_ref;
                $send_folder = new TargetFolderIdType();
                $send_folder->DistinguishedFolderId = new DistinguishedFolderIdType();
                $send_folder->DistinguishedFolderId->Id = DistinguishedFolderIdNameType::SENT;
                $send_request->SavedItemFolderId = $send_folder;
                $send_response = $this->client->SendItem($send_request);
                $send_response_messages = $send_response->ResponseMessages->SendItemResponseMessage;
                foreach($send_response_messages as $send_res_msg){
                    if($send_res_msg->ResponseClass != ResponseClassType::SUCCESS){
                        throw Exception("Message Failed To Send!");
                    }
                }


            }
        }


    }

    private function get_mails_as_assoc(Email_Query $restirction = NULL){
        $request = new FindItemType();
        $request->ItemShape = new ItemResponseShapeType();
        $request->ItemShape->BaseShape = DefaultShapeNamesType::DEFAULT_PROPERTIES;
        $request->Traversal = ItemQueryTraversalType::SHALLOW;
        $request->ParentFolderIds = new NonEmptyArrayOfBaseFolderIdsType();
        $request->ParentFolderIds->DistinguishedFolderId = new DistinguishedFolderIdType();
        $request->ParentFolderIds->DistinguishedFolderId->Id = DistinguishedFolderIdNameType::INBOX;
        $request->SortOrder = new NonEmptyArrayOfFieldOrdersType();
        $request->SortOrder->FieldOrder = array();
        if($restirction != NULL){
            $query_limit = $restirction->limit;
            if($query_limit != NULL) {
                $request->IndexedPageItemView = new IndexedPageViewType();
                $request->IndexedPageItemView->BasePoint = $query_limit->basepoint;
                $request->IndexedPageItemView->Offset = $query_limit->offset;
                $request->IndexedPageItemView->MaxEntriesReturned = $query_limit->limit;
            }
            //TODO: other restrictive-type stuffs...
        }
        $order = new FieldOrderType();
        @$order->FieldURI->FieldURI = 'item:DateTimeReceived';
        $order->Order = 'Ascending';
        $request->SortOrder->FieldOrder[] = $order;
        $foundItems = $this->client->FindItem($request);
        $messages = $foundItems->ResponseMessages->FindItemResponseMessage[0]->RootFolder->Items->Message;
        $mailIds = array();
        foreach($messages as $message){
            $itemId = $message->ItemId;
            $email = new Email_Id($itemId->Id);
            array_push($mailIds, $email);
        }
        $message_assocs = array();
        foreach($mailIds as $mail_id){
            $message_id = $mail_id->get_message_id();
            $itemDef = new GetItemType();
            $itemDef->ItemShape = new ItemResponseShapeType();
            $itemDef->ItemShape->BaseShape = DefaultShapeNamesType::ALL_PROPERTIES;
            $itemDef->ItemIds = new NonEmptyArrayOfBaseItemIdsType();
            $itemDef->ItemIds->ItemId = new ItemIdType();
            $itemDef->ItemIds->ItemId->Id = $message_id;
            $message = $this->client->GetItem($itemDef);
            array_push($message_assocs, $message);
        }
        return $message_assocs;
    }

    private function assoc_to_email($assoc){
        $assoc = $assoc->ResponseMessages->GetItemResponseMessage[0]->Items->Message[0];
        $fromAddr = $assoc->From->Mailbox->EmailAddress;
        $fromName = $assoc->From->Mailbox->Name;
        $sender = new Contact($fromAddr, $fromName);
        $recipients = array();
        if($assoc->ToRecipients != null) {
            foreach ($assoc->ToRecipients->Mailbox as $m) {
                $addr = $m->EmailAddress;
                $name = $m->Name;
                $to = new Contact($addr, $name);
                array_push($recipients, $to);
            }
        }
        $contact_list = new Contact_List($recipients);
        $title = $assoc->Subject;
        $body = $assoc->Body->_;
        $headers = array();
        //Microsoft, please, InternetMessageHeaders->InternetMessageHeader??
        foreach($assoc->InternetMessageHeaders->InternetMessageHeader as $header){
            $h = new Header(
                $header->HeaderName,
                $header->_
            );
            array_push($headers, $h);
        }
        $email = new Email(
            $sender,
            $contact_list,
            $body,
            $title,
            $headers
        );
        return $email;
    }

    public function add_event(Event $e){
        $request = new CreateItemType();
        $request->Items = new NonEmptyArrayOfAllItemsType();
        $request->Items->CalendarItem = new CalendarItemType();
        $request->Items->CalendarItem->Subject = $e->get_subject();
        $request->Items->CalendarItem->Start = $e->get_start_date_time()->format('c');
        $request->Items->CalendarItem->End = $e->get_end_date_time()->format('c');
        $request->Items->CalendarItem->Body = new BodyType();
        $request->Items->CalendarItem->Body->BodyType = BodyTypeType::HTML;
        $request->Items->CalendarItem->Body->_ = $e->get_body();
        $request->Items->CalendarItem->Categories = new ArrayOfStringsType();
        $request->Items->CalendarItem->Categories->String = $e->get_categories();
        $request->Items->CalendarItem->Importance = new ImportanceChoicesType();
        $request->Items->CalendarItem->Importance->_ = ImportanceChoicesType::NORMAL;
        $request->Items->CalendarItem->Location = $e->get_location();
        $request->SendMeetingInvitations = CalendarItemCreateOrDeleteOperationType::SEND_TO_NONE;
        $response = $this->client->CreateItem($request);
    }

    public function remove_events(Event_Query $query = NULL){
        $events = $this->get_events($query);
        foreach($events as $event){
            $event_id = $event->get_id();
            $event_change_key = $event->get_change_key();
            $request = new DeleteItemType();
            $request->DeleteType = DisposalType::HARD_DELETE;
            $request->SendMeetingCancellations = CalendarItemCreateOrDeleteOperationType::SEND_TO_NONE;
            $item = new ItemIdType();
            $item->Id = $event_id;
            $item->ChangeKey = $event_change_key;
            $items = new NonEmptyArrayOfBaseItemIdsType();
            $items->ItemId = $item;
            $request->ItemIds = $items;
            $response = $this->client->DeleteItem($request);
        }
    }

    public function get_events_between_dates(DateTime $from, DateTime $to){
        $q = new Event_Query();
        $q->from = $from;
        $q->to = $to;
        return $this->get_events($q);
    }

    public function get_events(Event_Query $query = NULL){
        if($query == NULL){
            throw new Exception("ERROR : You must pass a Event_Query object to Mailbox::get_events()");
        }
        $request = new FindItemType();
        $request->ParentFolderIds = new NonEmptyArrayOfBaseFolderIdsType();
        $request->ItemShape = new ItemResponseShapeType();
        $request->ItemShape->BaseShape = DefaultShapeNamesType::ALL_PROPERTIES;
        $folder_id = new DistinguishedFolderIdType();
        $folder_id->Id = DistinguishedFolderIdNameType::CALENDAR;
        $request->ParentFolderIds->DistinguishedFolderId[] = $folder_id;
        $request->CalendarView = new CalendarViewType();
        if($query != NULL) {
            if($query->from != NULL) {
                $start_date = $query->from;
                $request->CalendarView->StartDate = $start_date->format('c');
            } else {
                //default value
                $request->CalendarView->StartDate = (new DateTime())->sub(new DateInterval("P1Y"))->format('c');
            }
            if($query->to != NULL) {
                $end_date = $query->to;
                $request->CalendarView->EndDate = $end_date->format('c');
            } else {
                $request->CalendarView->EndDate = (new DateTime())->add(new DateInterval("P1Y"))->format('c');
            }
        }
        $request->Traversal = ItemQueryTraversalType::SHALLOW;
        $response = $this->client->FindItem($request);
        $response_messages = $response->ResponseMessages->FindItemResponseMessage;
        $response_event_objects = array();
        foreach ($response_messages as $response_message) {
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                continue;
            }
            $items = $response_message->RootFolder->Items->CalendarItem;
            foreach ($items as $item) {
                $id = $item->ItemId->Id;
                $change_key = $item->ItemId->ChangeKey;
                $organizedBy = $item->Organizer;
                $subject = $item->Subject;
                //$body = $item->Body->_;
                $body = "";
                $categories = $item->Categories;
                $location = $item->Location;
                $start = new DateTime($item->Start);
                $end = new DateTime($item->End);
                $wasCancelled = $item->IsCancelled;
                if($query != NULL) {
                    //iterate and filter
                    if ($query->subject != NULL) {
                        if($subject != $query->subject) continue;
                    }
                    if ($query->location != NULL) {
                        if($location != $query->location) continue;
                    }
                }
                $event_object = new Event($id, $change_key, $organizedBy, $subject, $body, $categories, $location, $start, $end, $wasCancelled);
                array_push($response_event_objects, $event_object);
            }
        }
        return $response_event_objects;
    }
}
