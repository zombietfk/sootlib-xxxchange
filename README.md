<h2>How to use Sootlib/WS!</h2>

(the actual library can be found in `application/models/WS/*`, you don't need to use code igniter, but there are some cool examples in this project)

<h3>Creating a client</h3>

```
$client = new Client($ip, $username, $password, $version); //ip will probably be 10.10.0.11

$client->setCurlOptions(array(
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_SSL_VERIFYHOST => false
)); //if you are connecting via https, you probably want to set these

```

Once a client is created, you must create a Mailbox, which will be
theyou primary way of interfacing with the exchange server.

```
$mailbox = new Mailbox($client);
```

<h3>Query a mailbox for emails</h3>

<h4>The easy way</h4>

Use
 - `Mailbox::get_x_recent_mails(int)`
 - `Mailbox::get_most_recent_mail()`,
 - `Mailbox::get_mail_by_index(int)`,

<h4>The slightly harder (But more useful) way</h4>

To *retrieve* emails or calendar events, you must first create a `*_Query` object.

```
$query = new Email_Query();
```

You can set the properties on a `Email_Query` struct to narrow your searches.
Setting the value of the property to `NULL` will result in the limitation being
ignored.

For example, to only get unread emails

```
$query->is_read = false;
```

Using this query (with no other options) will return all unread emails.

In order to limit the results to the 100 most recent emails, do the following :

```
$query_limit = new Email_Query_Limit();
$query_limit->limit = 100;
$query->limit = $query_limit; //where $query is an instance of Email_Query
$mails = $mailbox->get_mails($query); //where $mailbox is an instance of Mailbox
```

There are other properties, but I recommend you just look at the code / ask me what is up
if the property names dont make it obvious.

<h3>Sending mail</h3>

In order to send an email, you must pass a list of recpients in `Contact_List`
`Contact_List` basically is just a wrapper for an array of `Contact` instances (surprise surprise),
and it's constructor just takes an array.

A `Contact` is a contact that takes an address (their actual email address) and a recpeint name (this can be anything).
`Contact` is immutable once constructed (Just because).

```
$eve = new Contact("Eve.Turnbull@nduc.nhs.uk","Eve");
$to_list = new Contact_List(
    array(
        $eve
    )
);
$from = new Contact("Sam.Judge@Vocare.nhs.uk","Judge-Kun");
$body = "Hello everyone \n \n \n (sent via Sootmail)";
$title = "This is just a test";
$mail = new Email($from,$to_list,$body,$title);
$mailbox->send_mail($mail);
```

<h3>Setting an event</h3>

Use `Mailbox::add_event(Event)` in order to add an event to the calendar.
Constructing a Event is a pain because it takes loads of arguments, but here is the gist of it

```
$event = new Event(
    new Contact("wor@geet.email", "this is the organizer emailaddress+name (probably the ownwer of the mailbox you are using)"),
    "subject",
    "location",
    new DateTime(), //the beginning of the event
    new DateTime(), //the end of the event
    false //wasCancelled - Usually you'll just want to set it to false. For adding events it has -zero- bascially zero impact.
);
$mailbox->add_event($event);
```

Ok, so that's not too bad, right? Just construct you Event and pass it along to your Mailbox's add_event function.

<h3>Getting AND Removing an event</h3>

<h4>I read the Email_Query section and it sucked</h4>

Use `Mailbox::get_events_between_dates(DateTime $from, DateTime $to)`

<h4>Fancy way</h4>

Like with emails, you can pass a `*_Query` instance in order to get the events you ie. `Mailbox::get_event(Event_Query)`.

`Event_Query` has `$from` and `$to` properites (this represents a time frame and NOT the recipients/creator),
which can be set to DateTime objects, as well as the `$location` and `$subject` fields.
