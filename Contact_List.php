<?php

namespace Sootlib\XXXChange;

class Contact_List {

    private $contacts;

    public function __construct(array $contacts){
        $this->contacts = $contacts;
    }

    public function get_contacts(){
        return $this->contacts;
    }

}