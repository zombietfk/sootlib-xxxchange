<?php
/**
 * Created by PhpStorm.
 * User: sam.judge
 * Date: 31/08/2017
 * Time: 08:52
 */

namespace Sootlib\XXXChange;

use JsonSerializable;

class Event implements JsonSerializable {

    private $id;
    private $changeKey;
    private $organizer;
    private $subject;
    private $location;
    private $startDateTime;
    private $endDateTime;
    private $wasCancelled;
    private $body;
    private $categories;

    public function __construct(
        $id,
        $changeKey,
        $organizer,
        $subject,
        $body,
        $location,
        $startDateTime,
        $endDateTime,
        $wasCancelled,
        $categories) {
        $this->id = $id;
        $this->changeKey = $changeKey;
        $this->body = $body;
        $this->organizer = $organizer;
        $this->subject = $subject;
        $this->location = $location;
        $this->startDateTime = $startDateTime;
        $this->endDateTime = $endDateTime;
        $this->wasCancelled = $wasCancelled;
        $this->categories = $categories;
    }

    public function get_id(){
        return $this->id;
    }

    public function get_change_key(){
        return $this->changeKey;
    }

    public function get_body(){
        return $this->body;
    }

    public function get_organizer(){
        return $this->organizer;
    }

    public function get_subject(){
        return $this->subject;
    }

    public function get_location(){
        return $this->location;
    }

    public function get_start_date_time(){
        return $this->startDateTime;
    }

    public function get_end_date_time(){
        return $this->endDateTime;
    }

    public function get_was_cancelled(){
        return $this->wasCancelled;
    }

    public function get_categories(){
        return $this->categories;
    }

    function jsonSerialize() {
        return get_object_vars($this);
    }
}