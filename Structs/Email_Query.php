<?php

namespace Sootlib\XXXChange\Structs;

class Email_Query {

    public function __construct(){}

    public $limit = NULL;
    public $is_read = NULL;
    public $from_date = NULL;
    public $to_date = NULL;

}