<?php

namespace Sootlib\XXXChange\Structs;

use jamesiarmes\PhpEws\Enumeration\IndexBasePointType;

class Email_Query_Limit {

    public function __construct() {}

    public $limit = 10;
    public $offset = 0;
    public $basepoint = IndexBasePointType::END;
}
