<?php
/**
 * Created by PhpStorm.
 * User: sam.judge
 * Date: 29/08/2017
 * Time: 09:13
 */

namespace Sootlib\XXXChange\Structs;

class Header {

    public function __construct($name, $value) {
        $this->name = $name;
        $this->value = $value;
    }

    public $name;
    public $value;

}