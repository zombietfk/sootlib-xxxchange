<?php

namespace Sootlib\XXXChange\Structs;

class Email_Id{

    private $message_id;

    public function __construct($id){
        $this->message_id = $id;
    }

    public function get_message_id(){
        return $this->message_id;
    }
}