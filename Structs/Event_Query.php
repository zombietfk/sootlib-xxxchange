<?php
/**
 * Created by PhpStorm.
 * User: sam.judge
 * Date: 01/09/2017
 * Time: 09:40
 */

namespace Sootlib\XXXChange\Structs;

class Event_Query {
    public $location = NULL;
    public $subject = NULL;
    public $from = NULL;
    public $to = NULL;
}