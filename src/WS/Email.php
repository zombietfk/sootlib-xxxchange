<?php

namespace Sootlib\XXXChange;

use Exception;
use JsonSerializable;

class Email implements JsonSerializable {

    private $body;
    private $title;
    private $sender;
    private $recipients;
    private $headers;

    public function __construct(Contact $sender, Contact_List $recipients, $body, $title, array $headers = NULL){
        $this->recipients= $recipients;
        $this->sender = $sender;
        $this->body = $body;
        $this->title = $title;
        $this->headers = $headers;
    }

    public function get_reciever(){
        return $this->recipients;
    }

    public function get_sender(){
        return $this->sender;
    }

    public function get_header_value(string $headername){
        if($this->headers != NULL){
            foreach ($this->headers as $header){
                if($header->name == $headername){
                    return $header->value;
                }
            }
        }
        throw new Exception("ERROR : The header `$headername` could not be found in this email.");
    }

    public function get_body(){
        return $this->body;
    }

    public function get_title(){
        return $this->title;
    }
        
    public function get_bcc(){
      return explode(";", $this->headers["bcc"]);
    }
    
    public static function convert_bad_format_to_html($body, $format){
        $format = strtolower($format);
        switch($format){
            case "application/ms-tnef":
                $body = str_replace("<br>","",$body);
                $body = str_replace("<br />","",$body);
                break;
            default :
                break;
        }
        return $body;
    }

    function jsonSerialize() {
        return get_object_vars($this);
    }
}